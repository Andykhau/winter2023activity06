public class BoardGame
{
	//fields [PART TWO: QUESTION 1 AND 2]
	private Die dieOne;
	private Die dieTwo;
	private boolean[] tiles;
	
	//constructor [QUESTION 3]
	public BoardGame ()
	{
		this.dieOne = new Die();
		this.dieTwo = new Die();
		this.tiles = new boolean [12];
	}
	
	//toString [QUESTION 4]
	public String toString ()
	{
		String sequence = "Tiles = [";
		for(int i = 0; i <this.tiles.length; i++)
		{
			if(!(this.tiles[i]))
			{
				sequence = i + 1 + " ";
			}
			else
			{
				sequence = "X ";
			}
		}
		sequence += "]";
		return sequence;
	}
	
	//[QUESTION 5]
	public boolean playATurn()
	{
	dieOne.roll();
	dieTwo.roll();
	
	System.out.println(dieOne);
	System.out.println(dieTwo);
	
	int sumOfDice = dieOne.getFaceValue() + dieTwo.getFaceValue();
	
	if(!(tiles[sumOfDice - 1]))
	{
	System.out.println("Closing tile equal to sum: " + sumOfDice);
	tiles[sumOfDice - 1] = true;
	return false;
	}
	else if(!(tiles[sumOfDice - 1]))
	{
	System.out.println("Closing tile equal to the sum of die one: " + dieOne.getFaceValue());
	tiles[sumOfDice - 1] = true;
	return false;
	}
	else if(!(tiles[sumOfDice - 1]))
	{
	System.out.println("Closing tile equal to sum of die two: " + dieTwo.getFaceValue());
	tiles[sumOfDice - 1] = true;
	return false;
	}
	else
	{
		System.out.println("All the tiles for these values are already shut ");
		return true;
	}
	
	
	}		
}
