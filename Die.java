import java.util.Random;
public class Die
{
	private int faceValue;
	private static Random rnd = new Random();
	
	//constructor
	public Die()
	{
		this.faceValue = 1;
	}
	
	//get()
	public int getFaceValue()
	{
	return this.faceValue;
	}
	
	//roll()
	public void roll()
	{
	this.faceValue = rnd.nextInt(1,7);
	} 
	
	//toString()
	public String toString()
	{
		return "dice roll result: " + this.faceValue;
	}
}