public class Jackpot
{
public static void main (String[]args)
{
	System.out.println("Welcome to Andy's Board Game!");
	
	BoardGame Board = new BoardGame();
	boolean gameOver = false;
	int numOfTilesClosed = 0;
	
	while(!(gameOver))
	{
		System.out.println(Board);
		boolean check = Board.playATurn(); 
		if(check == true)
		{
			gameOver = true;	
		}
		else
		{
			numOfTilesClosed++;
	}
	}
	
	if(numOfTilesClosed >= 7)
	{
		System.out.println("JACKPOT!!! YOU WON THE GAME! CONGRATULATION!");
	}
	else{
	System.out.println("UNFORTUNATE YOU LOST! BETTER LUCK NEXT TIME! TO PLAY AGAIN GIVE ME YOUR WALLET!");
}
}
}